import { FetchResult } from "@apollo/client";
import { MockedProvider, MockedResponse } from "@apollo/client/testing";
import { ResultFunction } from "@apollo/client/utilities/testing/mocking/mockLink";
import { act, fireEvent, render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Route, Router } from "react-router";
import LibraryDisplay from "../../../components/LibraryDisplay/LibraryDisplay";
import { DELETE_LIBRARY, LIBRARIES_QUERY } from "../../../graphql-queries";
import Library from "../../../Interfaces/library";
import '@testing-library/jest-dom';

const defaultLibraries: Array<Library> = [
    {
        branch: 'New Kingston',
        id: 'anyvalidid'
    },
    {
        branch: 'Half Way Tree',
        id: 'anyvalidid2'
    }
];
const errorMessage = 'An error occurred';

const libraryQuery: ResultFunction<FetchResult<{
    [key: string]: any;
  }, Record<string, any>, Record<string, any>>> =  jest.fn(() => ({
    data: {
      libraries: defaultLibraries
    }
  }));

const deleteLibraryMutation: ResultFunction<FetchResult<{
    [key: string]: any;
  }, Record<string, any>, Record<string, any>>> = jest.fn(() => ({
    data: {
      deleteLibrary: true
    }
  }));


const getLibraryMocks =  (returnResult = true)  => {
    return [
        {
            request: {
                query: LIBRARIES_QUERY,
            },
            newData: returnResult ? libraryQuery : undefined,
            error: returnResult ? undefined : new Error(errorMessage)

        },
        {
            request: {
                query: DELETE_LIBRARY,
                variables: {
                    libraryId: defaultLibraries[0].id
                }
            },
            newData: returnResult ? deleteLibraryMutation : undefined,
            error: returnResult ? undefined : new Error(errorMessage)

        }
    ];
}

const renderLibraries = (mocks: readonly MockedResponse<Record<string, any>>[]) => {
    const history = createMemoryHistory({ initialEntries: [`/library/`] });
  
    return render(<Router history={history}>
         <MockedProvider mocks={mocks} addTypename={false}>
        <LibraryDisplay/>
    </MockedProvider>
    </Router>
   
    );
}


describe('Library Display', () => {

    it("Redirects to '' if library id is invalid", () => {
        const mocks = getLibraryMocks();
        const redirectUrl = '';
        const invalidLibraryId = 'invalidId';
        const history = createMemoryHistory({initialEntries: [`/library/${invalidLibraryId}`]});

        const renderResult = render(<Router history={history}>
            
            <Route path={'/library/:id'} render={ () =>
            <MockedProvider mocks={mocks} addTypename={false}>
            <LibraryDisplay/>
        </MockedProvider>}/>
        <Route path={redirectUrl} render={() => <>{redirectUrl}</>}></Route>
        </Router> );

        expect(renderResult.container.innerHTML.trim()).toEqual(redirectUrl);
    });

    it("Redirects to 'books/${libraryId}' if library id is invalid", () => {
        const mocks = getLibraryMocks();
        const validLibraryId = 'AnyValidId__';
        const redirectUrl = `/library/books/:${validLibraryId}`;
        const history = createMemoryHistory({initialEntries: [`/library/${validLibraryId}`]});

        const renderResult = render(<Router history={history}>
            <Route path={'/library/books/:id'} render={() => <>{redirectUrl}</>}></Route>
            <Route path={'/library/:id'} exact={true} render={ () =>
            <MockedProvider mocks={mocks} addTypename={false}>
            <LibraryDisplay/>
        </MockedProvider>}/>
        </Router> );

        expect(renderResult.container.innerHTML.trim()).toEqual(redirectUrl);
    });

    it('Loading screen is shown when page data is loading', () => {
        const mocks = getLibraryMocks();
    
        const libraryDisplay = renderLibraries(mocks);
    
        expect(libraryDisplay.container.firstElementChild?.classList.contains('loading__container')).toBe(true);
    
      });
    
    
      it('Error screen is shown when there is an error loading page data', async () => {
        const mocks = getLibraryMocks(false);
    
        renderLibraries(mocks);
    
        await act(async () => {
          await new Promise(resolve => setTimeout(resolve, 0));
        });
    
        const errorTextElement = screen.getByText(errorMessage);
    
        expect(errorTextElement).toBeInTheDocument();
      });
    
      describe('Displaying LIbraries', () => {
    
        const mocks = getLibraryMocks();
    
        beforeEach(async () => {
          renderLibraries(mocks);
    
          await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
          });
        })
    
        it('The number of libraries that are displayed matches number of library elements in response', async () => {
    
          const titleElements = await screen.findAllByTitle('branch');
    
          expect(titleElements).toHaveLength(defaultLibraries.length);
        });
    
        it('Library name/branch is displayed', async () => {
    
          const libraryTitleElement = await screen.findByText(defaultLibraries[0].branch);
    
          expect(libraryTitleElement).toBeInTheDocument();
        });
    
      });
    
      describe('Delete Modal', () => {
        const mocks = getLibraryMocks();
    
        beforeEach(async () => {
          jest.clearAllMocks();
          renderLibraries(mocks);
    
          await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
          });
    
          const deleteIcons = screen.getAllByTitle('delete__icon');
    
          if(deleteIcons.length === 0){
            fail('Cannot find delete icon');
          }
    
          const deleteIcon =  deleteIcons[0];
    
          act(() => {
            fireEvent.click(deleteIcon);
          });
    
        })
    
        it('Modal is displayed when delete icon is clicked', async () => {
          const deleteModal = screen.getByTitle('delete__modal');
          
          expect(deleteModal).toBeVisible();
        });
    
        it('Cancel button is displayed when delete icon is clicked', async () => {
          const cancelButton = await screen.findByText('Cancel');
    
          expect(cancelButton).toBeVisible();
        });
    
        it('Delete button is displayed when delete icon is clicked', async () => {
    
          const deleteButton = await screen.findByText('Delete');
    
          expect(deleteButton).toBeVisible(); 
        });
    
        it('Modal is dismissed when cancel button is clicked', async () => {
          const cancelButton = await screen.findByText('Cancel');
          const deleteModal =  await screen.findByTitle('delete__modal');
    
          act(() => {
            fireEvent.click(cancelButton);
          });
    
          expect(deleteModal).not.toBeInTheDocument();
        });
    
        it('When cancel button is clicked, delete mutation is not called', async () => {
          const cancelButton = await screen.findByText('Cancel');
    
          act(() => {
            fireEvent.click(cancelButton);
          });
    
          expect(deleteLibraryMutation).not.toHaveBeenCalled();
        });
    
        it('When delete button is clicked delete mutation is called', async () => {
          const deleteButton = await screen.findByText('Delete');
    
          await act(async () => {
            fireEvent.click(deleteButton);
            await new Promise(resolve => setTimeout(resolve, 0)); //For graphql mock provider to update
          });
    
          expect(deleteLibraryMutation).toHaveBeenCalledTimes(1);
    
        });
    
        it('When delete mutation is successful, get libraries is queried again', async () => {
          const deleteButton = await screen.findByText('Delete');
    
          await act(async () => {
            fireEvent.click(deleteButton);
            await new Promise(resolve => setTimeout(resolve, 0)); //For graphql mock provider to update
          });
    
          expect(libraryQuery).toHaveBeenCalledTimes(2);
    
        });
    
    
      });
    
});