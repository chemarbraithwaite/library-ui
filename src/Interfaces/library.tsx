class Library{
    id: string;
    branch: string;
    constructor(id: string, branch: string){

        if(!id || !branch){
            throw Error(`Invalid parameters id: ${id} name: ${branch}`);
        }

        this.id = id;
        this.branch = branch;
    }
}

export default Library;