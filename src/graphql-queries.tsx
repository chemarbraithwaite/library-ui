import { gql } from '@apollo/client';

export const LIBRARIES_QUERY = gql`
    query{
        libraries{
        id
        branch
        }
    }
`;

export const LIBRARY_BOOKS_QUERY = gql`
    query GetLibraryBooks($libraryId: String){
        getLibraryBooks(libraryId: $libraryId){
            id
            title
            author{
                name
            }
            year
            genre

        },
        getLibrary(id: $libraryId){
          branch
        }
    }
`;

export const ADD_BOOK = gql`
    mutation AddBookToLibrary($title: String!, $author: String!, $libraryId: String!, $year: Int!, $genre: [String]){
        createBook(input: {
        title: $title
        author: {
            name: $author
        },
        branchId: $libraryId
        year: $year
        genre: $genre
        }){
            id,
            title,
            author{
              name
            },
            year,
            genre,
            branchId
        }
    }
`;

export const ADD_LIBRARY = gql`
mutation AddLibrary($name: String!){
    createLibrary(branch: $name){
        id
    }
}
`;

export const DELETE_LIBRARY = gql`
    mutation DeleteLibrary($libraryId: String!){
        deleteLibrary(branchId: $libraryId)
    }
`;

export const DELETE_BOOK = gql`
    mutation DeleteLibrary($libraryId: String!, $bookId: String!){
        deleteBook(branchId: $libraryId, bookId: $bookId)
    }
`;