import { FormEventHandler, FunctionComponent, useState } from 'react';
import { OptionsType, OptionTypeBase } from 'react-select';
import Creatable from 'react-select/creatable'
import { Link, Redirect, useParams } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label, Card, CardBody, CardHeader, Row, Spinner, Alert } from 'reactstrap';
import { useMutation } from '@apollo/client';
import { ADD_BOOK } from '../../graphql-queries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';


const CreateBookForm: FunctionComponent = (props) => {


    const { libraryId } = useParams<{ libraryId: string }>();
    const [genres, setGenres] = useState<OptionsType<OptionTypeBase>>([]);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [title, setTitle] = useState<string>();
    const [author, setAuthor] = useState<string>();
    const [year, setYear] = useState<number>();
    const [isCreated, setIsCreated] = useState(false);

    const [addBook, {error, loading, client }] = useMutation(ADD_BOOK);

    const onGenreChange = async (values: OptionsType<OptionTypeBase>) => {
        setGenres(values);
    }

    const onFormSubmit: FormEventHandler<HTMLFormElement> = (event) => {
        event.stopPropagation();
        event.preventDefault();

        setIsSubmitting(true);

        addBook({
            variables: {
                genre: genres.map<string>(option => option.value),
                title: title,
                year: year,
                author: author,
                libraryId: libraryId
            }
        }).then(async() => {
            await client.clearStore();
            setIsCreated(true);
        })
        .catch(() => {
            setIsSubmitting(false);
        });
    }

    if (!libraryId) {
        return <Redirect to={'/library/'} />
    }

    if(isCreated){
       return <Redirect to={`/library/books/${libraryId}`}></Redirect>
    }

    return (
        <Card>
            <CardHeader><h2><Link to={`/library/books/${libraryId}`}><FontAwesomeIcon icon={faArrowCircleLeft}></FontAwesomeIcon></Link> Create Library Book</h2></CardHeader>
            <CardBody>
                <Row>
                    {
                        error && <Alert color='danger'>{error.message}</Alert>
                    }
                </Row>

                <Row>
                    <Form className='form__content' onSubmit={onFormSubmit}>
                        <FormGroup>
                            <Label for="title">Title</Label>
                            <Input disabled={isSubmitting} type='text' title='title' name="title" onChange={(event) => setTitle(event.target.value)} required={true} placeholder="Eat Pray Love" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="author">Author</Label>
                            <Input disabled={isSubmitting} type='text' name="author" title="author" onChange={(event) => setAuthor(event.target.value)} required={true} placeholder="Elizabeth Gilbert" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="year">Year</Label>
                            <Input disabled={isSubmitting} type='number' name="year" title="year" onChange={(event) => setYear(parseInt(event.target.value))} required={true} placeholder="2006" />
                        </FormGroup>
                        <FormGroup>
                            <Label>Genre</Label>
                            <Creatable disabled={isSubmitting} title='genre' onChange={onGenreChange} value={genres} placeholder={'Memoir'} className='' isClearable={false} isMulti={true} />
                        </FormGroup>
                        <Button disabled={isSubmitting}>{loading ? <Spinner/> : 'Create'}</Button>
                    </Form>
                </Row>

            </CardBody>
        </Card>
    );
}

export default CreateBookForm;