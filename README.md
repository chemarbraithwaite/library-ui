[![pipeline status](https://gitlab.com/chemarbraithwaite/library-ui/badges/master/pipeline.svg)](https://gitlab.com/chemarbraithwaite/library-ui/-/commits/master) [![coverage report](https://gitlab.com/chemarbraithwaite/library-ui/badges/master/coverage.svg)](https://gitlab.com/chemarbraithwaite/library-ui/-/commits/master)

# Library Front-end
This is a simple application for managing libraries and books. It was developed to practice using the [Apollo Client](https://www.apollographql.com/docs/react/) for [React](https://reactjs.org/).

The project was done in [TypeScript](https://www.typescriptlang.org/).

## Run this project

To run this project locally:

1. Start the [server](https://gitlab.com/chemarbraithwaite/library-server). See instruction on setting up and running the server [here](https://gitlab.com/chemarbraithwaite/library-server).
2. Run `npm install` in the root directory to install dependencies.
3. Run `npm run start` in the root directory

# Screenshots

### Library

![Alt Library](/screenshots/library.PNG?raw=true)

### Books

![Alt Book](/screenshots/book.PNG?raw=true)
